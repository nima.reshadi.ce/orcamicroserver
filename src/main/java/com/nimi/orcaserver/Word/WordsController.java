package com.nimi.orcaserver.Word;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;


@RestController
public class WordsController {



    @Autowired
    WordRepository wordRepository;

    @Autowired
    private Environment environment;

    @RequestMapping(value = "/getwords", method = {RequestMethod.GET})
    @ResponseBody
    public WordEntity getEnList(@RequestParam(value = "code", defaultValue = "") String code){
        WordEntity wordEntity = wordRepository.findByCode(code);

        wordEntity.setPort( environment.getProperty("local.server.port"));
        wordRepository.save(wordEntity);

        return wordEntity;

    }
}
